#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(impl_trait_in_assoc_type)]

use defmt::*;
use {defmt_rtt as _, panic_probe as _};

use embassy_executor::Spawner;
use embassy_rp::i2c::{self, Config};
use embassy_time::{Duration, Timer};

use eeprom24x::{Eeprom24x, SlaveAddr};
use heapless::Vec;
use panic_probe::hard_fault;

// AT24C08 8Kbit (1024byte)
const E_PAGE_SIZE: usize = 16;

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
    let p = embassy_rp::init(Default::default());

    let sda = p.PIN_14;
    let scl = p.PIN_15;

    let i2c = i2c::I2c::new_blocking(p.I2C1, scl, sda, Config::default());

    let mut eeprom = Eeprom24x::new_24x08(i2c, SlaveAddr::default());

    let memory_address = 0x0000; // From 0x0000 to 0x03FF

    // Read from eeprom
    let mut retreived_data: [u8; 1024] = [0; 1024];
    let _ = eeprom.read_data(memory_address, &mut retreived_data); // WARN Result ignored

    let mut result: Vec<u8, 1024> = Vec::new();

    result.extend_from_slice(&retreived_data).unwrap();

    let mut mem = 0;
    for val in result.chunks(E_PAGE_SIZE) {
        info!("[{:04}] {:#04X}", mem, val);
        mem += E_PAGE_SIZE as u32;
        let delay = Duration::from_millis(10);
        Timer::after(delay).await;
    }

    let _dev = eeprom.destroy(); // Get the I2C device back

    hard_fault();
}
