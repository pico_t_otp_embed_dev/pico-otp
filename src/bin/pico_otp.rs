#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(impl_trait_in_assoc_type)]

use defmt::*;
use {defmt_rtt as _, panic_probe as _};

use embassy_executor::Spawner;
use embassy_rp::{
    gpio::{Input, Pull},
    Peripheral,
};
use embassy_time::{Duration, Timer};

use pico_otp::{hid_kbd::hid_kbd, usb_net::usb_net};

#[derive(PartialEq)]
// Define an enumeration to represent the possible app states
enum AppState {
    // HidKbd mode represents the state where the application is acting as a HID keyboard
    HidKbd,
    // UsbNet mode represents the state where the application is in configuration mode by acting as a USB network device
    UsbNet,
}

#[embassy_executor::main]
async fn main(spawner: Spawner) {
    // Initialize pico peripherals
    let mut p = embassy_rp::init(Default::default());

    // Create an input object for the button pin (PIN_22) with a pull-up configuration
    let button = unsafe { Input::new(p.PIN_22.clone_unchecked(), Pull::Up) };

    // Introduce a delay to debounce the button press
    Timer::after(Duration::from_millis(250)).await;

    // Determine the application state based on the button state
    let app_state: AppState = match button.is_high() {
        // If the button is high, set the app state to HidKbd
        true => AppState::HidKbd,
        // If the button is low, set the app state to UsbNet
        false => AppState::UsbNet,
    };

    // Run the application in the determined mode (HidKbd or UsbNet)
    match app_state {
        // Application is running in HidKbd mode
        AppState::HidKbd => {
            info!("HidKbd state !!!");
            hid_kbd(&spawner, &mut p, button).await;
        }
        // Application is running in UsbNet mode
        AppState::UsbNet => {
            info!("UsbNet state !!!");
            usb_net(&spawner, &mut p).await;
        }
    }
}
