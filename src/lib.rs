#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(impl_trait_in_assoc_type)]

use arbitrary_int::*;
use bitbybit::bitfield;

pub mod hid_kbd;
pub mod tasks;
pub mod usb;
pub mod usb_net;

// Application bitfield options
#[bitfield(u8, default: 0)]
pub struct AppOptions {
    #[bit(7, rw)]
    pub otp: bool, // One-time password mode

    #[bits(3..=6, rw)]
    pub otp_remaining_tries: u4, // Number of remainings password use before eeprom erase

    #[bits(0..=2, rw)] // Keyboard Layout Number: 0 = us, 1 = frca etc...
    pub keyboard_layout: u3,
}

#[allow(non_camel_case_types)]
pub struct EE_PASS;
impl EE_PASS {
    pub const ADDR: u32 = 0;
    pub const SIZE: u32 = 1021;
    pub const LEN_ADDR: u32 = 1021;
}

#[allow(non_camel_case_types)]
pub struct EE_OPTS;
impl EE_OPTS {
    pub const ADDR: u32 = 1023;
    pub const SIZE: u32 = 1;
}
