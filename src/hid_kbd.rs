// use arbitrary_int::*;
use defmt::*;
use {defmt_rtt as _, panic_probe as _};

use core::str;
use core::sync::atomic::{AtomicBool, Ordering};

use embassy_executor::Spawner;
use embassy_futures::join::join;
use embassy_rp::gpio::Input;
use embassy_rp::Peripherals;
use embassy_time::{Duration, Timer};
use embassy_usb::class::hid::{HidReaderWriter, ReportId, RequestHandler, State as HidState};
use embassy_usb::control::OutResponse;
use embassy_usb::Handler;

use arbitrary_int::u4;
use heapless::String;
use static_cell::make_static;
use usbd_hid::descriptor::{KeyboardReport, SerializedDescriptor};

// My own library
use ee24x08::{self as EE};
use kbd::{kbd_frca::FrCaKbd, kbd_us::UsKbd, Keyboard};

use crate::{
    usb::{create_usb_builder, UsbMode},
    AppOptions,
};
use crate::{EE_OPTS, EE_PASS};

pub async fn hid_kbd(_spawner: &Spawner, p: &mut Peripherals, mut button: Input<'static>) {
    // Create eeprom 24x08 device
    let mut ee_device = EE::create_24x08_eeprom(p);

    // Create embassy-usb DeviceBuilder
    let mut usb_builder = create_usb_builder(p, UsbMode::HID);

    // Attach device handler call back
    let device_handler = make_static!(MyDeviceHandler::new());
    usb_builder.handler(device_handler);

    let request_handler = make_static!(MyRequestHandler {});
    let hid_state: &mut HidState<'_> = make_static!(HidState::new());

    // Create classes on the builder.
    let hid_config = embassy_usb::class::hid::Config {
        report_descriptor: KeyboardReport::desc(),
        request_handler: None,
        poll_ms: 10,
        max_packet_size: 64,
    };

    let hid = HidReaderWriter::<_, 1, 8>::new(&mut usb_builder, hid_state, hid_config);

    // Build the usb from builder
    let mut usb = usb_builder.build();

    // Run the USB device.
    let usb_fut = usb.run();

    // Enable the schmitt trigger to slightly debounce.
    button.set_schmitt(true);
    const DEBONCE: u64 = 150;

    // Split to hid reader and writer
    let (reader, mut writer) = hid.split();

    // Get keyboard layout from eeprom
    let app_opts = AppOptions::new_with_raw_value(ee_device.read_byte(EE_OPTS::ADDR).unwrap());
    let kb_type = u8::from(app_opts.keyboard_layout());
    let kbd: Keyboard = match kb_type {
        0 => Keyboard::Us(UsKbd),
        1 => Keyboard::FrCa(FrCaKbd),
        _ => Keyboard::Us(UsKbd),
    };

    // Do stuff with the class!
    let in_fut = async {
        loop {
            // Read AppOptions from eeprom
            let app_opts =
                AppOptions::new_with_raw_value(ee_device.read_byte(EE_OPTS::ADDR).unwrap());

            // Wait bouton press
            info!("Waiting for LOW on button");
            Timer::after(Duration::from_millis(DEBONCE)).await; // debonce
            button.wait_for_low().await;
            info!("LOW DETECTED");

            // check if one-time password is enabled
            if app_opts.otp() {
                info!(
                    " One-time password mode on !!! {}",
                    u8::from(app_opts.otp_remaining_tries())
                );

                let mut nb_try_remaining = u8::from(app_opts.otp_remaining_tries());

                if nb_try_remaining > 0 {
                    nb_try_remaining -= 1;

                    // Save nb try remaining
                    let app_opts = app_opts.with_otp_remaining_tries(u4::new(nb_try_remaining));
                    EE::write_buf_len(
                        &mut ee_device,
                        EE_OPTS::ADDR,
                        app_opts.raw_value as u32,
                        EE::ADDR_SIZE::OneByte,
                    )
                    .await;
                } else {
                    info!("No more password available !!!");

                    // Erase password area
                    let erase_data: [u8; EE_PASS::SIZE as usize] = [255; EE_PASS::SIZE as usize];
                    EE::write_buf(&mut ee_device, &erase_data, EE_PASS::ADDR).await;

                    // Write false password to eeprom
                    let s: String<30> = String::try_from("No more password available !!!").unwrap();
                    EE::write_buf(&mut ee_device, s.as_bytes(), EE_PASS::ADDR).await;
                    EE::write_buf_len(
                        &mut ee_device,
                        EE_PASS::SIZE,
                        s.len() as u32,
                        EE::ADDR_SIZE::TwoBytes,
                    )
                    .await;
                }
            }

            // Read Password from eeprom and send it through the keyboard
            let pass_len: usize =
                EE::read_buf_len(&mut ee_device, EE_PASS::LEN_ADDR, EE::ADDR_SIZE::TwoBytes).await;
            let mut pass_data: [u8; EE_PASS::SIZE as usize] = [0; EE_PASS::SIZE as usize];
            EE::read_buf(&mut ee_device, &mut pass_data[..pass_len], EE_PASS::ADDR).await;

            // Convert pass_data bytes buffer to String
            let pass: String<1024> =
                String::try_from(str::from_utf8(&pass_data[..pass_len]).unwrap()).unwrap();

            // Send the password String to keyboard
            kbd.str_to_keyboard(&mut writer, &pass).await;
        }
    };

    let out_fut = async {
        reader.run(false, request_handler).await;
    };

    // Run everything concurrently.
    // If we had made everything `'static` above instead, we could do this using separate tasks instead.
    join(usb_fut, join(in_fut, out_fut)).await;
}

struct MyRequestHandler {}

impl RequestHandler for MyRequestHandler {
    fn get_report(&mut self, id: ReportId, _buf: &mut [u8]) -> Option<usize> {
        info!("Get report for {:?}", id);
        None
    }

    fn set_report(&mut self, id: ReportId, data: &[u8]) -> OutResponse {
        info!("Set report for {:?}: {=[u8]}", id, data);
        OutResponse::Accepted
    }

    fn set_idle_ms(&mut self, id: Option<ReportId>, dur: u32) {
        info!("Set idle rate for {:?} to {:?}", id, dur);
    }

    fn get_idle_ms(&mut self, id: Option<ReportId>) -> Option<u32> {
        info!("Get idle rate for {:?}", id);
        None
    }
}

struct MyDeviceHandler {
    configured: AtomicBool,
}

impl MyDeviceHandler {
    fn new() -> Self {
        MyDeviceHandler {
            configured: AtomicBool::new(false),
        }
    }
}

impl Handler for MyDeviceHandler {
    fn enabled(&mut self, enabled: bool) {
        self.configured.store(false, Ordering::Relaxed);
        if enabled {
            info!("Device enabled");
        } else {
            info!("Device disabled");
        }
    }

    fn reset(&mut self) {
        self.configured.store(false, Ordering::Relaxed);
        info!("Bus reset, the Vbus current limit is 100mA");
    }

    fn addressed(&mut self, addr: u8) {
        self.configured.store(false, Ordering::Relaxed);
        info!("USB address set to: {}", addr);
    }

    fn configured(&mut self, configured: bool) {
        self.configured.store(configured, Ordering::Relaxed);
        if configured {
            info!(
                "Device configured, it may now draw up to the configured current limit from Vbus."
            )
        } else {
            info!("Device is no longer configured, the Vbus current limit is 100mA.");
        }
    }
}
