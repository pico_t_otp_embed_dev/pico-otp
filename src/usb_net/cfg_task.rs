use defmt::*;
use {defmt_rtt as _, panic_probe as _};

use core::ops::DerefMut;

use embassy_net::tcp::{Error as TcpError, TcpSocket};
use embassy_net::Stack;
use embassy_sync::{blocking_mutex::raw::NoopRawMutex, mutex::Mutex};
use embassy_time::{Duration, Timer};
use embassy_usb::class::cdc_ncm::embassy_net::Device as CdcNcmNetDevice;
use embedded_io_async::Write;

use arbitrary_int::{u3, u4};
use heapless::{String, Vec};

// My own library
use ee24x08::{self as EE, Ee24x08};
use kbd::KeyboardLayout;

use crate::usb_net::{bbblink, MTU};
use crate::AppOptions;
use crate::{EE_OPTS, EE_PASS};

// Check CRLF
fn is_only_crlf(buf: &[u8]) -> bool {
    match buf {
        [0x0a, ..] => true,      // LF
        [0xd, 0x0a, ..] => true, // CRLF
        _ => false,              // Have data
    }
}

// Trim CRLF if found
fn trim_crlf(buf: &[u8], mut n: usize) -> usize {
    // CRLF
    if n > 0 {
        // Remove LF
        if buf[n - 1] == 0x0a {
            n -= 1;
        }
        // Remove CR
        if n > 0 && buf[n - 1] == 0x0d {
            n -= 1;
        }
    }

    n
}

/// Checks if the given buffer represents a number between 0 and 15.
///
/// This function examines the buffer and returns an `Option<u8>` indicating the
/// extracted value from 0 to 15
///
/// # Parameters
/// * `buf`: The buffer to check
/// * `n`: The length of the buffer.
///
/// # Returns
/// An `Option<u8>` containing the parsed value or None
///
fn match_0_to_15(buf: &[u8]) -> Option<u8> {

    match (buf.len(), buf) {
        (1, [b'0'..=b'9']) => Some(buf[0] - b'0'),
        (2, [b'1', b'0'..=b'5']) => Some(buf[1] - b'0' + 10),
        _ => None,
    }
}

/// Sends a buffer over a TCP socket.
///
/// # Arguments
///
/// * `socket` - A mutable reference to the TCP socket.
/// * `buf` - The buffer to send.
///
/// # Returns
///
/// Returns `Ok(())` if the buffer was sent successfully, otherwise returns a `TcpError`.
///
async fn net_send_buf(socket: &mut TcpSocket<'_>, buf: &[u8]) -> Result<(), TcpError> {
    match socket.write_all(buf).await {
        Ok(()) => {}
        Err(e) => return Err(e),
    };

    Ok(())
}

/// Reads data from a TCP socket into a buffer.
///
/// # Arguments
///
/// * `socket` - A mutable reference to the TCP socket.
/// * `buf` - The buffer to read into.
/// * `skip_crlf_one` - A boolean indicating whether to skip the first CRLF sequence encountered.
///
/// # Returns
///
/// Returns the number of bytes read if successful, otherwise returns a `TcpError`.
///
async fn net_read_buf(
    socket: &mut TcpSocket<'_>,
    buf: &mut [u8],
    skip_crlf_one: bool,
) -> Result<usize, TcpError> {
    let mut crlf_once = !skip_crlf_one;

    let n = loop {
        let n = match socket.read(buf).await {
            // EOF
            Ok(0) => 0,
            Ok(n) => n,
            Err(e) => return Err(e),
        };

        if n > 1 && is_only_crlf(buf) && crlf_once {
            crlf_once = false;
            continue;
        };
        break n;
    };

    Ok(n)
}

/// Sends a message over a TCP socket.
///
/// # Arguments
///
/// * `socket` - A mutable reference to the TCP socket.
/// * `msg` - The message to send.
///
/// # Returns
///
/// Returns `Ok(())` if the message was sent successfully, otherwise returns a `TcpError`.
///
async fn send_msg(socket: &mut TcpSocket<'_>, msg: &str) -> Result<(), TcpError> {
    let msg: &Vec<u8, 64> = &String::try_from(msg).unwrap().into_bytes();

    match net_send_buf(socket, msg).await {
        Ok(_) => match socket.flush().await {
            Ok(_) => Ok(()),
            Err(e) => {
                warn!("socket flush error : {} for [{}]", e, msg);
                Err(e)
            }
        },
        Err(e) => {
            warn!("write [{}] error: {}", msg, e);
            Err(e)
        }
    }
}

/// Asks a question and reads the response from a TCP socket.
///
/// # Arguments
///
/// * `socket` - A mutable reference to the TCP socket.
/// * `msg` - The question to ask.
/// * `buf` - The buffer to read the response into.
/// * `skip_crlf_one` - A boolean indicating whether to skip the first CRLF sequence encountered.
///
/// # Returns
///
/// Returns the number of bytes read if successful, otherwise returns a `TcpError`.
///
async fn ask_and_read(
    socket: &mut TcpSocket<'_>,
    msg: &str,
    buf: &mut [u8],
    skip_crlf_one: bool,
) -> Result<usize, TcpError> {
    // Ask..
    match send_msg(socket, msg).await {
        Ok(_) => (),
        Err(e) => {
            warn!("write [{}] error: {}", msg, e);
            return Err(e);
        }
    }

    // Read it
    let mut n = match net_read_buf(socket, buf, skip_crlf_one).await {
        Ok(n) => n,
        Err(e) => {
            warn!("read [{}] error: {}", msg, e);
            return Err(e);
        }
    };

    n = trim_crlf(buf, n);

    Ok(n)
}

#[embassy_executor::task]
pub async fn cfg_task(
    mtx_ee: &'static Mutex<NoopRawMutex, Ee24x08>,
    stack: &'static Stack<CdcNcmNetDevice<'static, MTU>>,
) -> ! {
    // And now we can use it!
    let mut rx_buffer = [0; 4096];
    let mut tx_buffer = [0; 4096];
    let mut buf = [255u8; 4096];

    'read_socket_loop: loop {
        let mut socket = TcpSocket::new(stack, &mut rx_buffer, &mut tx_buffer);

        socket.set_timeout(Some(embassy_time::Duration::from_secs(30)));

        info!("Listening on TCP:1234...");

        if let Err(e) = socket.accept(1234).await {
            warn!("accept error: {:?}", e);
            continue 'read_socket_loop;
        }

        info!("Received connection from {:?}", socket.remote_endpoint());

        // Ask for password
        let mut n: usize = match ask_and_read(&mut socket, "Enter password: ", &mut buf, true).await
        {
            Ok(n) => n,
            Err(_) => continue 'read_socket_loop,
        };

        if n > EE_PASS::SIZE as usize {
            n = EE_PASS::SIZE as usize;
        }

        match &buf[..n] {
            b"config otp" => {
                info!("Config otp");

                // Ask for number of retry needed
                let n = match ask_and_read(
                    &mut socket,
                    "Enter the number of attempts required (0-15): ",
                    &mut buf,
                    false,
                )
                .await
                {
                    Ok(n) => n,
                    Err(_) => continue 'read_socket_loop,
                };

                // Trap CTRL-C
                if n == 0 {
                    socket.abort();
                    continue 'read_socket_loop;
                }

                // Get number from 0 to 15 only
                let nb_att: Option<u8> = match_0_to_15(&buf[..n]);

                // If not LF or CRLF and is a number from 0 to 15 go
                if !is_only_crlf(&buf) && nb_att.is_some() {
                    info!("Ok set otp on and save nb attemps to eeprom !!!");

                    // Read actual AppOptions from eeprom
                    let app_opts = AppOptions::new_with_raw_value(
                        mtx_ee.lock().await.read_byte(EE_OPTS::ADDR).unwrap(),
                    );

                    // Disable otp
                    if nb_att == Some(0) {
                        // Create a updated AppOtions and write to eeptom
                        let app_opts = app_opts
                            .with_otp(false)
                            .with_otp_remaining_tries(u4::new(0));
                        EE::write_buf_len(
                            mtx_ee.lock().await.deref_mut(),
                            EE_OPTS::ADDR,
                            app_opts.raw_value as u32,
                            EE::ADDR_SIZE::OneByte,
                        )
                        .await;
                    } else {
                        // Create a updated AppOtions and write to eeptom
                        let app_opts = app_opts
                            .with_otp(true)
                            .with_otp_remaining_tries(u4::new(nb_att.unwrap()));
                        EE::write_buf_len(
                            mtx_ee.lock().await.deref_mut(),
                            EE_OPTS::ADDR,
                            app_opts.raw_value as u32,
                            EE::ADDR_SIZE::OneByte,
                        )
                        .await;
                    }

                    // Done
                    let _ = send_msg(&mut socket, "Saving to eeprom completed !!!\r\n").await;
                    bbblink();
                } else {
                    info!("No attempts chosen, boo !!!");
                    let _ = send_msg(&mut socket, "No attempts chosen, boo !!!\r\n").await;
                }
            }

            b"config keyboard" => {
                info!("Config Keyboard layout");

                // Ask for keyboard layout
                let n = match ask_and_read(
                    &mut socket,
                    "Enter keyboard layout (us, frca): ",
                    &mut buf,
                    false,
                )
                .await
                {
                    Ok(n) => n,
                    Err(_) => continue 'read_socket_loop,
                };

                // Trap CTRL-C
                if n == 0 {
                    socket.abort();
                    continue 'read_socket_loop;
                }

                if is_only_crlf(&buf) {
                    info!("You entered no keyboard layout info, boo !!!");
                    let _ = send_msg(&mut socket, "Bad keyboard layout, boo !!!\r\n").await;
                } else {
                    info!("LAYOUT = {:#04X}, n = {}", buf[..n], n);
                    Timer::after(Duration::from_millis(10)).await;

                    let kl: KeyboardLayout = match &buf[..] {
                        [0x75, 0x73, ..] if n == 2 => KeyboardLayout::Us,
                        [0x66, 0x72, 0x63, 0x61, ..] if n == 4 => KeyboardLayout::FrCa,
                        _ => {
                            let _ = send_msg(&mut socket, "Bad keyboard layout, boo !!!\r\n").await;
                            continue 'read_socket_loop;
                        }
                    };

                    // Read actual AppOptions from eeprom
                    let app_opts = AppOptions::new_with_raw_value(
                        mtx_ee.lock().await.read_byte(EE_OPTS::ADDR).unwrap(),
                    );

                    // Create a updated AppOtions and write to eeptom
                    let app_opts = app_opts.with_keyboard_layout(u3::new(kl as u8));
                    EE::write_buf_len(
                        mtx_ee.lock().await.deref_mut(),
                        EE_OPTS::ADDR,
                        app_opts.raw_value as u32,
                        EE::ADDR_SIZE::OneByte,
                    )
                    .await;

                    // Done
                    let _ = send_msg(&mut socket, "Saving to eeprom completed !!!\r\n").await;
                    bbblink();
                }
            }

            _ => {
                // Not CRLF or LF go forward or boo !!!
                if !is_only_crlf(&buf) {
                    info!("Save password to eeprom !!!");
                    info!("PASS = {:#04X} n = {}", &buf[..n], n);

                    // Trap CTRL-C
                    if n == 0 {
                        socket.abort();
                        continue 'read_socket_loop;
                    }

                    // Set otp remaining tries to 1 by default when exausted and a new password is recorded (because otp mode is true).
                    let app_opts = AppOptions::new_with_raw_value(
                        mtx_ee.lock().await.read_byte(EE_OPTS::ADDR).unwrap(),
                    );
                    if app_opts.otp() && app_opts.otp_remaining_tries() == u4::new(0) {
                        let app_opts = app_opts.with_otp_remaining_tries(u4::new(1));
                        EE::write_buf_len(
                            mtx_ee.lock().await.deref_mut(),
                            EE_OPTS::ADDR,
                            app_opts.raw_value as u32,
                            EE::ADDR_SIZE::OneByte,
                        )
                        .await;
                    }

                    // Erase password area
                    let erase_data: [u8; EE_PASS::SIZE as usize] = [255; EE_PASS::SIZE as usize];
                    EE::write_buf(mtx_ee.lock().await.deref_mut(), &erase_data, EE_PASS::ADDR)
                        .await;

                    // Write password to eeprom
                    EE::write_buf(mtx_ee.lock().await.deref_mut(), &buf[..n], EE_PASS::ADDR).await;

                    // Memorise size on last 2 eerpom byte
                    EE::write_buf_len(
                        mtx_ee.lock().await.deref_mut(),
                        EE_PASS::LEN_ADDR,
                        n as u32,
                        EE::ADDR_SIZE::TwoBytes,
                    )
                    .await;

                    // Done
                    let _ = send_msg(&mut socket, "Saving to eeprom completed !!!\r\n").await;
                    bbblink();
                } else {
                    info!("You entered no password, boo !!!");
                    let _ = send_msg(&mut socket, "No password, boo !!!\r\n").await;
                }
            }
        }
    }
}
