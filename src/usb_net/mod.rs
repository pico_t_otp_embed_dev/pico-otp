use defmt::*;
use {defmt_rtt as _, panic_probe as _};

use embassy_executor::Spawner;
use embassy_net::{Ipv4Address, Ipv4Cidr, Stack, StackResources};
use embassy_rp::gpio::{Level, Output};
use embassy_rp::{Peripheral, Peripherals};
use embassy_sync::{blocking_mutex::raw::NoopRawMutex, mutex::Mutex};
use embassy_usb::class::cdc_ncm::embassy_net::{Device as CdcNcmNetDevice, State as NetState};
use embassy_usb::class::cdc_ncm::{CdcNcmClass, State as CdcNcmState};

use heapless::Vec;
use portable_atomic::Ordering;
use static_cell::make_static;

#[cfg(feature = "pico-w")]
use {
    cyw43::Control,
    cyw43_pio::PioSpi,
    embassy_net_driver_channel::Device as WiFiNetChannelDevice,
    embassy_rp::bind_interrupts,
    embassy_rp::peripherals::PIO0,
    embassy_rp::pio::{InterruptHandler, Pio},
};

// My own library
use ee24x08::{self as EE, Ee24x08};

use crate::usb_net::cfg_task::cfg_task;
use crate::usb_net::web_task::web_task;
use crate::{tasks::*, usb::*};

pub mod cfg_task;
pub mod web_task;

pub const MTU: usize = 1514;

pub fn bbblink() {
    BLINK_LED.store(true, Ordering::Relaxed);
}

fn create_usb_net_task(
    spawner: &Spawner,
    p: &mut Peripherals,
    our_mac_addr: [u8; 6],
    host_mac_addr: [u8; 6],
) -> CdcNcmNetDevice<'static, MTU> {
    // Create embassy-usb DeviceBuilder using the driver and config.
    let mut usb_builder = create_usb_builder(p, UsbMode::CDC);

    usb_builder.function(0x03, 0x00, 0x00); // Set HID type to default

    // Create classes on the builder.
    let usb_class = CdcNcmClass::new(
        &mut usb_builder,
        make_static!(CdcNcmState::new()),
        host_mac_addr,
        64,
    );

    // Build the builder.
    let usb = usb_builder.build();

    unwrap!(spawner.spawn(usb_task(usb)));

    let (runner, net_device) =
        usb_class.into_embassy_net_device::<MTU, 4, 4>(make_static!(NetState::new()), our_mac_addr);
    unwrap!(spawner.spawn(usb_ncm_task(runner)));

    net_device
}

// WiFi
#[cfg(feature = "pico-w")]
bind_interrupts!(struct WiFiIrqs {
    PIO0_IRQ_0 => InterruptHandler<PIO0>;
});

#[cfg(feature = "pico-w")]
async fn initialize_wifi(
    spawner: &Spawner,
    p: &mut Peripherals,
) -> (WiFiNetChannelDevice<'static, 1514>, Control<'static>) {
    // WiFi cyw43 setup
    //
    let fw = include_bytes!("../../../../embassy/cyw43-firmware/43439A0.bin");
    let clm = include_bytes!("../../../../embassy/cyw43-firmware/43439A0_clm.bin");

    // To make flashing faster for development, you may want to flash the firmwares independently
    // at hardcoded addresses, instead of baking them into the program with `include_bytes!`:
    //     probe-rs download 43439A0.bin --format bin --chip RP2040 --base-address 0x10100000
    //     probe-rs download 43439A0_clm.bin --format bin --chip RP2040 --base-address 0x10140000
    //let fw = unsafe { core::slice::from_raw_parts(0x10100000 as *const u8, 224190) };
    //let clm = unsafe { core::slice::from_raw_parts(0x10140000 as *const u8, 4752) };

    let pwr = unsafe { Output::new(p.PIN_23.clone_unchecked(), Level::Low) };
    let cs = unsafe { Output::new(p.PIN_25.clone_unchecked(), Level::High) };
    let mut pio = unsafe { Pio::new(p.PIO0.clone_unchecked(), WiFiIrqs) };
    let spi = unsafe {
        PioSpi::new(
            &mut pio.common,
            pio.sm0,
            pio.irq0,
            cs,
            p.PIN_24.clone_unchecked(),
            p.PIN_29.clone_unchecked(),
            p.DMA_CH0.clone_unchecked(),
        )
    };

    let state = make_static!(cyw43::State::new());
    let (net_device, mut control, runner) = cyw43::new(state, pwr, spi, fw).await;
    unwrap!(spawner.spawn(wifi_task(runner)));

    control.init(clm).await;
    control
        .set_power_management(cyw43::PowerManagementMode::PowerSave)
        .await;

    (net_device, control)
}

pub async fn usb_net(spawner: &Spawner, p: &mut Peripherals) {
    // Pico W Led ON
    #[cfg(feature = "pico-w")]
    {
        let (_wifi_net_device, mut wifi_control) = initialize_wifi(spawner, p).await;
        wifi_control.gpio_set(0, true).await; // Pico W LED ON
        unwrap!(spawner.spawn(blink_led_task(wifi_control)));
    }

    // Pico Led ON
    #[cfg(not(feature = "pico-w"))]
    {
        let mut led = unsafe { Output::new(p.PIN_25.clone_unchecked(), Level::Low) };
        led.set_high();
        unwrap!(spawner.spawn(blink_led_task(led)));
    }

    // Our internal MAC addr.
    let our_mac_addr = [0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC];

    // Host's MAC addr. This is the MAC the host "thinks" its USB-to-ethernet adapter has.
    let host_mac_addr = [0x88, 0x88, 0x88, 0x88, 0x88, 0x88];

    // Start the USB net tasks
    let net_device = create_usb_net_task(spawner, p, our_mac_addr, host_mac_addr);

    //let config = embassy_net::Config::dhcpv4(Default::default());
    let net_config = embassy_net::Config::ipv4_static(embassy_net::StaticConfigV4 {
        address: Ipv4Cidr::new(Ipv4Address::new(169, 254, 123, 123), 16),
        dns_servers: Vec::new(),
        gateway: None,
    });

    // Generate random seed
    let seed = 29384729; // guaranteed random, chosen by a fair dice roll

    // Init network stack
    let stack = &*make_static!(Stack::new(
        net_device,
        net_config,
        make_static!(StackResources::<2>::new()),
        seed
    ));

    // Spawn network task
    unwrap!(spawner.spawn(net_task(stack)));

    // Create Mutex on the 24x08 eeprom device
    let mtx_ee: &mut Mutex<NoopRawMutex, Ee24x08> =
        make_static!(Mutex::new(EE::create_24x08_eeprom(p)));

    // Spawn config task port 1234
    unwrap!(spawner.spawn(cfg_task(mtx_ee, stack)));

    // Spawn web config task port 80
    unwrap!(spawner.spawn(web_task(mtx_ee, stack)));
}
