use defmt::*;
use {defmt_rtt as _, panic_probe as _};

use core::ops::DerefMut;

use embassy_net::Stack;
use embassy_sync::{blocking_mutex::raw::NoopRawMutex, mutex::Mutex};
use embassy_time::Duration;
use embassy_usb::class::cdc_ncm::embassy_net::Device as CdcNcmNetDevice;

use arbitrary_int::{u3, u4};
use picoserve::routing::get;
use serde::Deserialize;
use static_cell::make_static;

// My own libraries
use ee24x08::{self as EE, Ee24x08};
use kbd::KeyboardLayout;

use crate::usb_net::MTU;
use crate::AppOptions;
use crate::{EE_OPTS, EE_PASS};


// Define a custom timer implementation for picoserve
struct EmbassyTimer;

impl picoserve::Timer for EmbassyTimer {
    type Duration = embassy_time::Duration;
    type TimeoutError = embassy_time::TimeoutError;

    async fn run_with_timeout<F: core::future::Future>(
        &mut self,
        duration: Self::Duration,
        future: F,
    ) -> Result<F::Output, Self::TimeoutError> {
        embassy_time::with_timeout(duration, future).await
    }
}

// Define a struct to hold form data
#[derive(Deserialize)]
struct FormValue {
    otp: i32,
    pass: heapless::String<{ EE_PASS::SIZE as usize }>,
    kbd: heapless::String<4>,
}

#[embassy_executor::task]
pub async fn web_task(
    mtx_ee: &'static Mutex<NoopRawMutex, Ee24x08>,
    stack: &'static Stack<CdcNcmNetDevice<'static, MTU>>,
) -> ! {
    let form_router = &picoserve::Router::new().route(
        "/",
        get(|| picoserve::response::File::html(include_str!("index.html"))).post(
            |picoserve::extract::Form(FormValue { otp, pass, kbd })| async move {
                // Create an AppOtions instance and write it to EEPROM
                let otp_active = otp > 0;
                let kl: KeyboardLayout = match kbd.as_str() {
                    "Us" => KeyboardLayout::Us,
                    "FrCa" => KeyboardLayout::FrCa,
                    _ => KeyboardLayout::Us,
                };
                let app_opts = AppOptions::DEFAULT;
                let app_opts = app_opts
                    .with_otp(otp_active)
                    .with_otp_remaining_tries(u4::new(otp as u8))
                    .with_keyboard_layout(u3::new(kl as u8));
                EE::write_buf_len(
                    mtx_ee.lock().await.deref_mut(),
                    EE_OPTS::ADDR,
                    app_opts.raw_value as u32,
                    EE::ADDR_SIZE::OneByte,
                )
                .await;

                // Save password to EEPROM
                //
                let n = pass.len();
                // Erase password area
                let erase_data: [u8; EE_PASS::SIZE as usize] = [255; EE_PASS::SIZE as usize];
                EE::write_buf(mtx_ee.lock().await.deref_mut(), &erase_data, EE_PASS::ADDR).await;

                // Write password to EEPROM
                EE::write_buf(
                    mtx_ee.lock().await.deref_mut(),
                    pass.as_bytes(),
                    EE_PASS::ADDR,
                )
                .await;

                // Memorise size on last 2 EEPROM byte
                EE::write_buf_len(
                    mtx_ee.lock().await.deref_mut(),
                    EE_PASS::LEN_ADDR,
                    n as u32,
                    EE::ADDR_SIZE::TwoBytes,
                )
                .await;

                //picoserve::response::DebugValue((("otp", otp), ("pass", pass), ("kbd", kbd)))
                "Saving to eeprom completed !!!"
            },
        ),
    );

    // Create a static config for picoserve
    let picoserve_config = make_static!(picoserve::Config::new(picoserve::Timeouts {
        start_read_request: Some(Duration::from_secs(5)),
        read_request: Some(Duration::from_secs(1)),
        write: Some(Duration::from_secs(1)),
    })
    .keep_connection_alive());

    // Allocate buffers for TCP socket needed by networking communication (Rx/Tx)
    let mut rx_buffer = [0; 1024];
    let mut tx_buffer = [0; 1024];

    // Start an infinite loop to handle incoming connections
    loop {
        let mut tcp_socket = embassy_net::tcp::TcpSocket::new(stack, &mut rx_buffer, &mut tx_buffer);

        info!("Listening on TCP:80...");
        if let Err(e) = tcp_socket.accept(80).await {
            warn!("Accept error: {:?}", e);
            continue;
        }

        info!("Received connection from {:?}", tcp_socket.remote_endpoint());

        let (tcp_socket_rx, tcp_socket_tx) = tcp_socket.split();

        // Handle the web request
        match picoserve::serve(
            form_router,
            EmbassyTimer,
            picoserve_config,
            &mut [0; 2048],
            tcp_socket_rx,
            tcp_socket_tx,
        )
        .await
        {
            Ok(_) => {
                info!("Requests handled from {:?}", tcp_socket.remote_endpoint());
            }
            Err(_) => error!("Request error !!! "),
        }
    }
}
