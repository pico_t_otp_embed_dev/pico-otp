use embassy_rp::peripherals::{self, USB};
use embassy_rp::usb::{Driver, InterruptHandler as UsbInterruptHandler};
use embassy_rp::{bind_interrupts, Peripheral, Peripherals};
use embassy_usb::{Builder as UsbBuilder, Config as UsbConfig};

use static_cell::make_static;

pub enum UsbMode {
    HID,
    CDC,
}

pub const USB_VENDOR_ID: u16 = 0xC0DE;
pub const USB_PRODUCT_ID: u16 = 0xCAFE;

pub type UsbDriver = Driver<'static, peripherals::USB>;

bind_interrupts!(pub struct UsbIrqs {
    USBCTRL_IRQ => UsbInterruptHandler<USB>;
});

pub fn create_usb_builder(
    p: &mut Peripherals,
    usb_mode: UsbMode,
) -> UsbBuilder<'static, UsbDriver> {
    // Create the driver, from the HAL.
    let usb_driver = unsafe { Driver::new(p.USB.clone_unchecked(), UsbIrqs) };

    let mut usb_config = UsbConfig::new(USB_VENDOR_ID, USB_PRODUCT_ID);
    usb_config.manufacturer = Some("Ivan Arsenault");
    usb_config.product = Some("Pico OTP");
    usb_config.device_release = 0x102; // Ex: bcdDevice= 1.02
    usb_config.serial_number = None;
    usb_config.max_power = 100;
    usb_config.max_packet_size_0 = 64;

    match usb_mode {
        UsbMode::HID => {
            // Required for Windows support.
            usb_config.composite_with_iads = true;
            usb_config.device_class = 0xEF;
            usb_config.device_sub_class = 0x02;
            usb_config.device_protocol = 0x01;
        }
        UsbMode::CDC => {
            // Required for Windows support.
            usb_config.composite_with_iads = false;
            usb_config.device_class = 0xEF;
            usb_config.device_sub_class = 0x04;
            usb_config.device_protocol = 0x01;
        }
    }

    // Create embassy-usb DeviceBuilder using the driver and config.
    let usb_builder = UsbBuilder::new(
        usb_driver,
        usb_config,
        &mut make_static!([0; 256])[..], // config descriptor
        &mut make_static!([0; 256])[..], // bos descriptor
        &mut make_static!([0; 256])[..], // msos descriptor
        &mut make_static!([0; 128])[..], // control buffer
    );

    usb_builder
}
