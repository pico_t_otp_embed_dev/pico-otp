use embassy_net::Stack;
use embassy_rp::gpio::Output;
use embassy_time::{Duration, Timer};
use embassy_usb::class::cdc_ncm::embassy_net::{Device as CdcNcmNetDevice, Runner};
use embassy_usb::UsbDevice;

use portable_atomic::{AtomicBool, Ordering};

#[cfg(feature = "pico-w")]
use {
    cyw43::Control,
    cyw43_pio::PioSpi,
    embassy_rp::peripherals::{DMA_CH0, PIO0},
};

use crate::usb::UsbDriver;
use crate::usb_net::MTU;

const BLINK_SPEED: u64 = 180;

pub static BLINK_LED: AtomicBool = AtomicBool::new(false); // Blink led task indicator

#[embassy_executor::task]
pub async fn usb_task(mut device: UsbDevice<'static, UsbDriver>) -> ! {
    device.run().await
}

#[embassy_executor::task]
pub async fn usb_ncm_task(class: Runner<'static, UsbDriver, MTU>) -> ! {
    class.run().await
}

#[embassy_executor::task]
pub async fn net_task(stack: &'static Stack<CdcNcmNetDevice<'static, MTU>>) -> ! {
    stack.run().await
}

#[cfg(feature = "pico-w")]
#[embassy_executor::task]
pub async fn wifi_task(
    runner: cyw43::Runner<'static, Output<'static>, PioSpi<'static, PIO0, 0, DMA_CH0>>,
) -> ! {
    runner.run().await
}

#[cfg(not(feature = "pico-w"))]
#[embassy_executor::task]
pub async fn blink_led_task(mut led: Output<'static>) {
    loop {
        if BLINK_LED.load(Ordering::Relaxed) {
            for _ in 0..6 {
                Timer::after(Duration::from_millis(BLINK_SPEED)).await;
                led.toggle();
            }

            BLINK_LED.store(false, Ordering::Relaxed);
        }

        Timer::after(Duration::from_millis(1)).await;
    }
}

#[cfg(feature = "pico-w")]
#[embassy_executor::task]
pub async fn blink_led_task(mut wifi_control: Control<'static>) {
    let mut toggle = false;

    loop {
        if BLINK_LED.load(Ordering::Relaxed) {
            for _ in 0..6 {
                wifi_control.gpio_set(0, toggle).await;
                Timer::after(Duration::from_millis(BLINK_SPEED)).await;
                toggle = !toggle;
            }

            BLINK_LED.store(false, Ordering::Relaxed);
        }

        Timer::after(Duration::from_millis(1)).await;
    }
}
