# Rasbpberry Pi Pico (W) One Time Password

## Overview

This project utilizes a Raspberry Pi Pico with RP2040 microcontroller to generate One-Time Passwords (OTP) and emulate a keyboard to directly output the password into a computer.

## Features

**OTP Generation**: The Raspberry Pi Pico sent a OTP password to the computer by emulating a keyboard 

## Setup

1. **Hardware Requirements**:
   - Raspberry Pi Pico with RP2040 microcontroler
   - Push button
   - EEPROM 24C08
   - Pico Probe

2. **Software Dependencies**:
   - Rust
   - Embassy, https://embassy.dev/

3. **Installation**:
   - Clone this repository
   - Install the required dependencies with git: https://github.com/embassy-rs/embassy
   - Use Pico Probe: https://www.raspberrypi.com/products/debug-probe/
   - cargo run --release --bin pico_otp

4. **Configuration**:
   - Boot with push button activated
   - Go to http://169.254.123.123
   - Enter password

5. **Usage**:
   - Connect the Raspberry Pi Pico to your computer via USB.
   - Push button to send password

## Security Considerations

- **Security**: Safeguard the device to prevent unauthorized use of the password

## License

This project is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)

## Acknowledgements

- [Embassy](https://embassy.dev/)

## Contributing

Contributions are welcome! Please feel free to submit issues or pull requests.
